import numpy as np
from math import sqrt

# from geometry_msgs.msg import PolygonStamped
from geometry_msgs.msg import Point32
from geometry_msgs.msg import Point # ros2 docs recommend using this instead of Point32
from geometry_msgs.msg import PoseWithCovarianceStamped
from geometry_msgs.msg import PoseWithCovariance
from geometry_msgs.msg import Pose
from nav_msgs.msg import Odometry

# # only needed if we need to transform lidar to local cartesian frame in this node
# from tf2_geometry_msgs import tf2_geometry_msgs
# from tf2.LinearMath import Quaternion

# from voltron_msgs.msg import ConeBearings

from voltron_msgs.msg import OneCone, ManyCones

from visualization_msgs.msg import Marker, MarkerArray

from std_srvs.srv import Empty

class Subscriber(Node):
    def __init__(self):
        super().__init__('the_map_node')

        self.publisher_ = self.create_publisher(ManyCones, 'the_map', 10)
        self.marker_publisher_ = self.create_publisher(MarkerArray, 'the_map_markers', 10)
        self.the_map_ = ManyCones()
        self.cones_ = []
        self.the_map_markers_ = MarkerArray()
        self.markers_ = []
        self.next_marker_id = 0

        self.cone_subscription = self.create_subscription(
            ManyCones,
            'lidar_camera',
            self.lc_cones_callback,
            10)

        self.dumb_loc_subscription = self.create_subscription(
            Odometry,
            'odometry/gps',
            self.dumb_callback,
            10)

        self.cone_subscription # prevent unused variable warning

        self.dumb_loc_subscription # prevent unused variable warning

        self.service = self.create_service(Empty, '/the_map', self.create_map)
        timer_period = 0.5 # seconds
        self.timer = self.create_timer(timer_period, self.track_pub_timer_call)

    def create_map(self, _request, response):
        if self.the_map_ == None:
            return

        # If we have lapped, reset the map
        if is_near_start_pos():
            self.the_map_ = ManyCones()
            self.cones_ = []
            self.the_map_.cones = self.cones_

            self.the_map_markers_ = MarkerArray()
            self.markers_ = []
            self.the_map_markers_.markers = self.markers_
            return

        if self.cone_msg_in_ != None:
            in_cones_list = self.cone_msg_in_.cones
            for aCone in in_cones_list:
                cones_.append(aCone)
                markers_.append(gen_marker_from_cone(cone, cone_msg_in_.stamp))

            self.the_map_ = ManyCones()
            self.the_map_.header = Header()
            self.the_map_.header.stamp = self.get_clock().now().to_msg()
            self.the_map_.header.frame_id = "map"
            self.the_map_.cones = self.cones_

            self.the_map_markers_ = MarkerArray()
            self.the_map_markers_.markers = self.markers_

        return response

    def is_near_start_pos():
        PROXIMITY_THRESHOLD = 5.0

        if self.dumb_loc_ == None:
            return True
        else:
            position = dumb_loc.pose.pose.position
            x, y = position.x, position.y
            dist = sqrt(x * x + y * y)
            return (dist < PROXIMITY_THRESHOLD)

    def gen_marker_from_cone(cone, stamp):
        marker = Marker()
        marker.header.frame_id="map"
        marker.header.stamp = stamp
        marker.ns = "my_namespace"
        marker.id = self.next_marker_id # must be unique within namespace
        self.next_marker_id += 1
        marker.type = Marker.CYLINDER
        marker.action = Marker.ADD
        marker.pose.position.x, marker.pose.position.y, marker.pose.position.z = cone.point.x, cone.point.y, 0.0
        marker.pose.orientation.x, marker.pose.orientation.y, marker.pose.orientation.z = 0.0, 0.0, 0.0
        marker.scale.x, marker.scale.y, marker.scale.z = 0.1, 0.1, 0.25
        marker.color.a = 1.0
        marker.color.r, marker.color.g, marker.color.b = 0.914, 0.416, 0.0 # orange
        return marker


    def lc_cones_callback(self, msg):
        self.cone_msg_in_ = msg

    def dumb_callback(self, msg):
        self.dumb_loc_ = msg

    def track_pub_timer_call(self):
        self.publisher_.publish(self.the_map_)
        self.marker_publisher_.publish(self.the_map_markers_)

def main(args=None):
    rclpy.init(args=args)

    subscriber = Subscriber()

    rclpy.spin(subscriber)

    subscriber.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
