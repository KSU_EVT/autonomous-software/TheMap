import os
from setuptools import setup
from setuptools import find_packages
import setuptools
from glob import glob

package_name = 'the_map'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml'])
    ],
    install_requires=[
        'setuptools',
        'numpy',
        'ros2'
    ],
    zip_safe=True,
    maintainer='matthew',
    maintainer_email='mkrupcza@students.kennesaw.edu',
    description='creates a map of cone detections. resets after every lap',
    license='Unlicense',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'the_map = the_map.the_map:main',
        ],
    },
)
